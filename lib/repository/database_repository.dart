import 'package:empty_fridge/repository/shared/repository.dart';
import 'package:empty_fridge/services/database_service.dart';

class DatabaseRepository extends Repository<DatabaseRepository> {
  // region [Initialize]
  DatabaseRepository._privateConstructor();

  static final DatabaseRepository _instance = DatabaseRepository._privateConstructor();

  static DatabaseRepository get instance => _instance;
  // endregion

  Future<String> getGallery() => DatabaseService.instance.getGallery();
}