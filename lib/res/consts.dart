import 'package:logger/logger.dart';

// region [Routing]
const String ROUTE_SPLASH_SCREEN  = '/splash_screen';
// endregion

// region [MAIN]
const String EMPTY_STRING = '';
const String DATA = 'data';
// endregion

// region [Language]
const String BASE_LOCALE = 'en';
const String EN_LOCALE = 'en';
// endregion

// region [ScreenUtil]
const bool DESIGN_SCREEN_ALLOW_FONT_SCALING = true;
const double DESIGN_SCREEN_HEIGHT = 640.0;
const double DESIGN_SCREEN_WIDTH = 360.0;
// endregion

var logger = Logger();
