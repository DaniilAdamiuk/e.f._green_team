abstract class Request<T> {
  // ignore: missing_return
  Future<T> send() async {}
}