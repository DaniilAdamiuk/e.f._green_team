import 'package:empty_fridge/store/app/app_state.dart';
import 'package:empty_fridge/store/shared/initialization/initialize_actions.dart';
import 'package:redux/redux.dart';

class InitializeSelectors {
  static void startInitialization(Store<AppState> store) {
    return () {
      store.dispatch(StartInitialization());
    }();
  }
}