import 'package:flutter/material.dart';

abstract class Dialog {
  Widget widget();

  void show();
}