import 'package:empty_fridge/helpers/dialog_view.dart';
import 'package:empty_fridge/store/shared/loader/loader_state.dart';
import 'package:empty_fridge/store/shared/interfaces/loader.dart';
import 'package:empty_fridge/ui/dialog/default_lodaer_dialog_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DefaultLoaderDialog implements LoaderDialog {
  @override
  final bool state;

  @override
  final LoaderKey loaderKey;

  @override
  final String title;

  DefaultLoaderDialog({
    @required this.state,
    @required this.loaderKey,
    this.title,
  });

  @override
  Widget widget() => DefaultLoaderDialogWidget(this);

  @override
  void show() => DialogView.instance.show(this);
}
