import 'package:empty_fridge/store/app/app_state.dart';
import 'package:empty_fridge/store/shared/loader/loader_state.dart';
import 'package:redux/redux.dart';

class LoaderSelectors {
  static bool getValueForLoadingKey(Store<AppState> store, LoaderKey loaderKey) {
    int index = store.state.loaderState.loaders.indexWhere((loader) => loader.loaderKey == loaderKey);

    return index != -1 ? true : false;
  }
}
