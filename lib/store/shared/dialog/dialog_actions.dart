import 'package:empty_fridge/store/shared/base_action.dart';
import 'package:empty_fridge/store/shared/interfaces/dialog.dart' as di;
import 'package:flutter/material.dart';

class ShowDialog extends BaseAction {
  final di.Dialog dialog;

  ShowDialog({
    @required this.dialog,
  }) : super(type: 'ShowDialog');
}
