import 'package:empty_fridge/res/typedef.dart';
import 'package:empty_fridge/store/app/app_state.dart';
import 'package:empty_fridge/store/shared/language/language_actions.dart';
import 'package:redux/redux.dart';

class LanguageSelector {
  static String getSelectedLocale(Store<AppState> store) {
    return store.state.languageState.locale;
  }

  static ChangeLanguageFunction changeLanguage(Store<AppState> store) {
    return (String locale) {
      store.dispatch(
        ChangeLanguageAction(locale: locale),
      );
    };
  }
}
