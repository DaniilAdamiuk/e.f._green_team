import 'package:flutter/material.dart';
import 'package:empty_fridge/store/shared/interfaces/dialog.dart' as dialog;
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

class DialogView {
  // region [Initialize]
  static const String TAG = '[Repository]';

  DialogView._privateConstructor();

  static final DialogView _instance = DialogView._privateConstructor();

  static DialogView get instance => _instance;
  // endregion

  void show(dialog.Dialog dialog) {
   showDialog(
     context: NavigatorHolder.navigatorKey?.currentState?.overlay?.context,
     builder: (BuildContext ctx) {
       return dialog.widget();
     },
   );
  }
}