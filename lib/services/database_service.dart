import 'package:empty_fridge/network/requests/database/get_gallery_request.dart';
import 'package:empty_fridge/services/shared/service.dart';

class DatabaseService extends Service<DatabaseService>{
  // region [Initialize]
  DatabaseService._privateConstructor();

  static final DatabaseService _instance = DatabaseService._privateConstructor();

  static DatabaseService get instance => _instance;
  // endregion

  Future<String> getGallery() async => await GetGalleryRequest().send();
}
